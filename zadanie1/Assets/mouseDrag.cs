using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseDrag : MonoBehaviour
{
	private Vector3 startMousePosition;
	private void OnMouseDown()
	{
		startMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}
	private void OnMouseDrag()
	{
		transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition - startMousePosition);
	}
}
