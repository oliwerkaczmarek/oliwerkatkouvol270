using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towerControl : MonoBehaviour
{
    private towerRotation tr;
	private void Start()
	{
		tr = this.GetComponent<towerRotation>();
	}
	void Update()
    {
        if(tr.towerRotations == 12)
		{
			tr.canRotate = false;
			tr.towerRotations = 0;
		}
    }

	public void ResetTower()
	{
		tr.towerRotations = 0;
		tr.canRotate = true;
	}


}
