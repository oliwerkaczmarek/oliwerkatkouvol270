using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class towerRotation : MonoBehaviour
{
	public bool canRotate = true;
	public int nextRotation = 25;
	[SerializeField] private GameObject bulletPref;
	[SerializeField] private Transform shootPoint;
	public int towerRotations = 0;
	private Quaternion startShootPointRotation;
	private void Start()
	{
		startShootPointRotation = shootPoint.rotation;
	}
	private void FixedUpdate()
	{
		if(canRotate)
		{
			this.GetComponent<SpriteRenderer>().color = Color.red;
			if(nextRotation > 0)
			{
				nextRotation--;
			}
			else
			{
				transform.Rotate(0,0,Random.Range(15,45)+transform.rotation.z);
				towerRotations += 1;
				shoot();
				nextRotation = 25;
			}
		}
		else
		{
			this.GetComponent<SpriteRenderer>().color = Color.white;
		}
	}

	private void shoot()
	{
		GameObject bullet = Instantiate(bulletPref, shootPoint.position, startShootPointRotation);
		bullet.GetComponent<Rigidbody2D>().AddForce(shootPoint.up * 4,ForceMode2D.Impulse);
	}
}
