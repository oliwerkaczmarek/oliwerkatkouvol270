using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class towerCounting : MonoBehaviour
{
    public int towers = 1;
	private void Update()
	{
		towers = GameObject.FindGameObjectsWithTag("Player").Length;
		this.GetComponent<Text>().text = "Towers: " + towers.ToString();

		if(towers == 100)
		{
			for(int i = 0; i < GameObject.FindGameObjectsWithTag("Player").Length;i++)
			{
				GameObject.FindGameObjectsWithTag("Player")[i].GetComponent<towerControl>().ResetTower();
			}
		}

	}
}
