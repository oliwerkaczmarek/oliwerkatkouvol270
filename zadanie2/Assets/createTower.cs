using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createTower : MonoBehaviour
{
	private int stopDistance;
	[SerializeField] private GameObject towerPref;
	private Vector3 startPosition;
	private void Start()
	{
		startPosition = transform.position;
		stopDistance = Random.Range(1, 4);
	}
	void Update()
    {
        if(Vector3.Distance(startPosition, transform.position)>=stopDistance)
		{
			PlaceTower();
			Destroy(this.gameObject);
		}
    }

	private void PlaceTower()
	{
		if(GameObject.FindGameObjectWithTag("Respawn").GetComponent<towerCounting>().towers < 100)
		{
			GameObject tower = Instantiate(towerPref, transform.position, transform.rotation);
			tower.GetComponent<towerRotation>().nextRotation = 300;
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.name == "Tower(Clone)")
		{
			Destroy(collision.gameObject);
			Destroy(this.gameObject);
		}
	}
}
